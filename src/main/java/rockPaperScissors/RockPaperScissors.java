package rockPaperScissors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    public String randomChoice(){
        Random choice = new Random();
        String random = rpsChoices.get(choice.nextInt(rpsChoices.size()));
        return (random);
    }

    public static boolean isWinner(String choice1, String choice2){
        if (choice1 == "paper"){
            return (choice2 == "rock");
        }
        else if (choice1 == "scissors"){
            return (choice2 == "paper");
        }
        return (choice2 == "scissors");
    }

    public String userChoice(){
       while (true){
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?"); 
            if (rpsChoices.contains(userChoice)){
                return userChoice;
            }
            else{
                System.out.println("I do not understand " + userChoice + ". Could you try again?");
            }
        }
    }
    
    public String continuePlaying(){
        while (true){
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?");
            List <String> acceptAnswer = new ArrayList<String>();
                acceptAnswer.add("n");
                acceptAnswer.add("y");
            if (acceptAnswer.contains(continueAnswer)){
                return continueAnswer;
            }
            else{
                System.out.println("I don't understand " + continueAnswer + ". Could you try again?"); 
            }

        }
    }

    public void run() {
        while (true){
            System.out.println("Let's play round " + roundCounter);
            String playerChoice = userChoice();
            String computerChoice = randomChoice();
            

            if (playerChoice.equals(computerChoice)){
                System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + ". It's a tie!");
            }
            else if (computerChoice.equals("rock") && playerChoice.equals("scissors")){
                System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }
            else if (computerChoice.equals("paper") && playerChoice.equals("rock")){
                System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }
            else if (computerChoice.equals("scissors") && playerChoice.equals("paper")){
                System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + ". Computer wins!");
                computerScore ++;
            }
            else{
                System.out.println("Human chose " + playerChoice + ", computer chose " + computerChoice + ". Human wins!");
                humanScore ++;
            }
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);


            String continueAnswer = continuePlaying();
            if (continueAnswer.equals("y")){
                roundCounter ++;
            }
            else if (continueAnswer.equals("n")){
                System.out.println("Bye bye :)");
                break; 
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}